﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace HomeTask4
{
    public class LibraryService : ILibraryService
    {
        private ILibraryCallback Callback => OperationContext.Current.GetCallbackChannel<ILibraryCallback>();
        private static Person _currentPerson;
        private static ICollection<Book> _requestedBooks;
        private static List<Book> _returnedBooks;
        private static readonly ICollection<LibraryItem> Library = new List<LibraryItem>()
        {
            new LibraryItem { OnHand = true, Book = new Book { Id = 1, Author = "Loyd A. G.", BookType = Book.BookTypes.Magazine, Name = "Guide for fats", Year = 322}},
            new LibraryItem { OnHand = false, Book = new Book { Id = 2, Author = "Author1", BookType = Book.BookTypes.Scientific, Name = "Book1"}},
            new LibraryItem { OnHand = true, Book = new Book { Id = 3, Author = "Author2", BookType = Book.BookTypes.Scientific, Name = "Book2"}},
            new LibraryItem { OnHand = false, Book = new Book { Id = 4, Author = "Author3", BookType = Book.BookTypes.Scientific, Name = "Book3"}},
            new LibraryItem { OnHand = false, Book = new Book { Id = 5, Author = "Author4", BookType = Book.BookTypes.Scientific, Name = "Book4"}},
        };
        private static readonly Dictionary<int, ICollection<Order>> Orders = new Dictionary<int, ICollection<Order>>()
        {
            {1, new List<Order>
                {
                    new Order { OrderCreateTime = DateTime.Now.AddMonths(-2), BookId = 1 }
                }
            },
            {2, new List<Order>()
            {
                new Order { OrderCreateTime = DateTime.Now.AddMonths(-2), BookId = 3 }
            } }
        };

        public void JoinLibrary(Person person)
        {
            _currentPerson = person;
            _requestedBooks = new List<Book>();
            _returnedBooks = new List<Book>();
            Console.WriteLine($"Hello, {person.FIO}! You have {Orders[_currentPerson.Id].Count} books");
        }

        public void ReturnBooks(List<Book> books)
        {
            _returnedBooks.AddRange(books);
        }

        public ICollection<string> GetBooks()
        {
            return Library.Select(x => x.Book.Name).ToList();
        }

        public ICollection<Book> Apply()
        {
            foreach (var returned in _returnedBooks)
            {
                Orders[_currentPerson.Id].Remove(Orders[_currentPerson.Id]
                    .Single(x => x.BookId.Equals(returned.Id)));
                Library.Single(x => x.Book.Id == returned.Id).OnHand = false;
            }
            Console.WriteLine($"{_returnedBooks.Count} books returned.");

            foreach (var request in _requestedBooks)
            {
                Library.Single(x => x.Book.Id == request.Id).OnHand = true;
                Orders[_currentPerson.Id].Add(
                    new Order { OrderCreateTime = DateTime.Now, BookId = request.Id });
            }
            Console.WriteLine($"{_requestedBooks.Count} books taken.");
            Console.WriteLine($"Now you have {Orders[_currentPerson.Id].Count} books.");
            Console.WriteLine($"Bye, {_currentPerson.FIO}!");
            Console.WriteLine();
            return _requestedBooks;
        }

        public void RequestBook(string sId)
        {
            var id = int.Parse(sId);
            if (Library.All(x => x.Book.Id != id))
            {
                var fault = new LibraryFault()
                {
                    FaultText = "В библиотеке нет такой книги!"
                };
                throw new FaultException<LibraryFault>(fault);
            }
            if (Library.Single(x => x.Book.Id == id).OnHand)
            {
                var fault = new LibraryFault()
                {
                    FaultText = "Эта книга уже занята!"
                };
                throw new FaultException<LibraryFault>(fault);
            }
            _requestedBooks.Add(Library.Single(x => x.Book.Id == id).Book);
        }

        public void Leave()
        {
            Console.WriteLine($"Bye, {_currentPerson.FIO}!");
        }
    }

    [DataContract]
    public class Book
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public BookTypes BookType { get; set; }

        public enum BookTypes
        {
            /// <summary>
            /// Журнал
            /// </summary>
            Magazine,
            /// <summary>
            /// Художественная книга
            /// </summary>
            Fiction,
            /// <summary>
            /// Научная
            /// </summary>
            Scientific
        }
    }

    public class Person
    {
        public int Id { get; set; }
        // ReSharper disable once InconsistentNaming
        public string FIO { get; set; }
    }

    public class LibraryItem
    {
        public bool OnHand { get; set; }

        public Book Book { get; set; }
    }

    public class Order
    {
        public DateTime OrderCreateTime { get; set; }

        public int BookId { get; set; }
    }
}