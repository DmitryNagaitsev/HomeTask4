﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace HomeTask4
{
    [ServiceContract]
    public interface ILibraryService
    {
        [OperationContract]
        [FaultContract(typeof(LibraryFault))]
        [WebInvoke(
            UriTemplate = "/request/{id}",
            Method = "POST")]
        void RequestBook(string id);

        [OperationContract]
        [WebInvoke(
            UriTemplate = "/join",
            RequestFormat = WebMessageFormat.Json,
            Method = "POST")]
        void JoinLibrary(Person person);

        [OperationContract]
        [WebInvoke(
            UriTemplate = "/return",
            RequestFormat = WebMessageFormat.Json,
            Method = "POST")]
        void ReturnBooks(List<Book> books);

        [OperationContract]
        [WebGet(
            UriTemplate = "/books",
            ResponseFormat = WebMessageFormat.Json)]
        ICollection<string> GetBooks();

        [OperationContract]
        [WebInvoke(
            UriTemplate = "/apply",
            Method = "POST")]
        ICollection<Book> Apply();

        [OperationContract(IsOneWay = true)]
        [WebInvoke(
            UriTemplate = "/leave",
            Method = "POST")]
        void Leave();
    }

    [ServiceContract]
    public interface ILibraryCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnCallback();
    }

    public class LibraryFault
    {
        [DataMember]
        public string FaultText { get; set; }
    }
}
