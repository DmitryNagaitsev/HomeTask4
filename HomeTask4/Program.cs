﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace HomeTask4
{
    public class Program
    {
        public static void Main()
        {
            var host = new ServiceHost(typeof(LibraryService));

            host.Open();

            Console.WriteLine("Press enter...");
            Console.ReadLine();

            host.Close();
        }
    }
}